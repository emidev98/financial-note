import moment from 'moment';
import { ModificationStatus } from './ModifiedStatus';

export class Entry {
    public id                   : string;
    public categoryId           : string;
    public description          : string;
    public date                 : string;
    public value                : number;
    public dataChanged          : boolean;
    public modificationStatus   : ModificationStatus = ModificationStatus.untouched;

    public getCurrentMonthExpenses() : number{
        
        if(moment(this.date).get("month") == moment().get("month")){
            return this.value;           
        }
        return 0;
    }
}
