import { Entry } from "./Entry";
import { ModificationStatus } from './ModifiedStatus';

export class Category {
    public id                   : string = '';
    public title                : string = '';
    public icon                 : string = '';
    public color                : string = '';
    public listIndex            : number;
    public currentMonthExpenses : number;
    public modificationStatus   : ModificationStatus = ModificationStatus.untouched;
    public entries              : Array<Entry> = new Array<Entry>();
    public expanded             : Boolean = false;

    public getMonthlyExpenses() : number{
        let currentMonthExpenses = 0;
        
        this.entries.forEach( entry => {
            currentMonthExpenses += entry.getCurrentMonthExpenses();
        });

        this.currentMonthExpenses = currentMonthExpenses;
        return this.currentMonthExpenses;
    }
}
