import { Category } from './Category';
import moment from 'moment';

export interface IFiltersModel{
    categories? : Category[];
    entries? : String[];
    startDate? : moment.Moment;
    endDate? : moment.Moment;
    vsLastDateOfSamePeriod? : boolean;
}