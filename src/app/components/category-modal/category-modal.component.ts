import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Category } from 'src/app/models/Category';
import { StylesService } from 'src/app/services/styles.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'app-category-modal',
    templateUrl: './category-modal.component.html',
    styleUrls: ['./category-modal.component.scss'],
})
export class CategoryModalComponent implements OnInit {
    
    public categoryForm : FormGroup;
    public category : Category;

    constructor(
        private formBuilder: FormBuilder,
        public stylesService : StylesService,
        public dialogRef: MatDialogRef<CategoryModalComponent>,
        @Inject(MAT_DIALOG_DATA) public data: {isNew : boolean,category : Category}
    ) {}

    ngOnInit() {
        this.category = this.data.category;
        this.createFormControls();
    }

    private createFormControls(){
        this.categoryForm = this.formBuilder.group({
            title : [this.category.title,[Validators.required]],
            color : [this.category.color,[Validators.required]],
            icon : [this.category.icon,[Validators.required]],
        });
    }

    public onClickSave(){
        this.category.title = this.categoryForm.value.title;
        this.category.color = this.categoryForm.value.color;
        this.category.icon = this.categoryForm.value.icon;
        this.dialogRef.close(this.category);
    }

    public onClickCloseModal(){
        this.dialogRef.close();
    }
}
