import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Entry } from 'src/app/models/Entry';
import { StylesService } from 'src/app/services/styles.service';
import { MAT_DIALOG_DATA, MatDialogRef, MAT_DATE_FORMATS, DateAdapter, MAT_DATE_LOCALE } from '@angular/material';
import { DatabaseService } from 'src/app/services/database.service';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import _ from "lodash";
import moment from "moment";
import { Category } from 'src/app/models/Category';

@Component({
    selector: 'app-entry-modal',
    templateUrl: './entry-modal.component.html',
    styleUrls: ['./entry-modal.component.scss'],
    providers: [
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
        { provide : MAT_DATE_FORMATS,
        useValue : {
            parse : {
                dateInput : "YYYY-MM-DD"
            },
            display: {
                dateInput: 'YYYY-MM-DD'
            }
        }
    }]
})
export class EntryModalComponent implements OnInit {

    public entryForm: FormGroup;
    public entry: Entry;
    public financialNotes : Category[];

    constructor(
        private formBuilder: FormBuilder,
        public stylesService: StylesService,
        public dialogRef: MatDialogRef<EntryModalComponent>,
        public databaseService : DatabaseService,
        @Inject(MAT_DIALOG_DATA) public data: { isNew: boolean, entry: Entry }
    ) { }

    ngOnInit() {
        this.entry = this.data.entry;
        this.financialNotes = this.databaseService.financialNotes;
        this.createFormControls();
    }

    private createFormControls(){
        let date = this.data.isNew ? new Date() : this.entry.date;
        this.entryForm = this.formBuilder.group({
            description : [this.entry.description,[Validators.required]],
            categoryId : [this.entry.categoryId,[Validators.required]],
            date : [date,[Validators.required]],
            value : [this.entry.value,[Validators.required]],
        });
    }

    public onClickSave(){
        this.entry.date = _.isDate(this.entryForm.value.date) ? moment(this.entryForm.value.date).format("YYYY-MM-DD") : this.entryForm.value.date;
        this.entry.description = this.entryForm.value.description;
        this.entry.categoryId = this.entryForm.value.categoryId;
        this.entry.value = this.entryForm.value.value;
        this.dialogRef.close(this.entry);
    }

    public onClickCloseModal(){
        this.dialogRef.close();
    }
}
