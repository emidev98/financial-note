import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ModesService {

    private editModeChangedSubject: Subject<boolean> = new Subject();
    private _isEditMode: boolean = false;
    public onEditModeChanged : Observable<boolean> = this.editModeChangedSubject.asObservable();

    constructor() { }

    get isEditMode(){
        return this._isEditMode;
    }

    set isEditMode(isEditMode: boolean){
        this.editModeChangedSubject.next(isEditMode);
        this._isEditMode = isEditMode;
    }
}
