import { CanActivate, Router } from "@angular/router";
import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable, Subscription } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {
    
    private authStateSubscription : Subscription;

    constructor(
        private fireAuth: AngularFireAuth
    ) { }

    canActivate() : Observable<boolean>{
        return new Observable( observer => {
            this.authStateSubscription = this.fireAuth.authState
                .subscribe((user) =>{
                    observer.next(user !== null);
                    this.authStateSubscription.unsubscribe();
                },()=>{
                    observer.next(false);
                    this.authStateSubscription.unsubscribe();
                })
            });
    }
}