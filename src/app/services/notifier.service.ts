import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class NotifierService {

    private updateRouteTitleSubject : Subject<{ title : string, icon? : string}> = new Subject<{ title : string, icon? : string}>();
    public defaultDuration          : number = 2000;
    public onUpdateRouteTitle       : Observable<{ title : string, icon? : string}> = this.updateRouteTitleSubject.asObservable();
    
    constructor(
        private snackBar: MatSnackBar,
        private translateService : TranslateService
    ) { }

    public updateRouteTitle(translateKey : string, iconKey? : string){
        let routeTitle = this.translateService.instant(translateKey);
        this.updateRouteTitleSubject.next({title : routeTitle, icon : iconKey});
    }

    public success(message: string, duration?: number) {
        this.show(message, "toast-success", duration);
    }

    public error(message: string, duration?: number) {
        this.show(message, "toast-error", duration);
    }

    private show(message, panelType, duration) {
        message = this.translateService.instant(message);
        
        this.snackBar.open(message,null,{
            panelClass : panelType,
            duration : duration ? duration : this.defaultDuration,
            verticalPosition : "bottom"
        });
    }
}
