import { Component, OnInit, Input } from '@angular/core';
import { Category } from 'src/app/models/Category';

@Component({
    selector: 'app-dimension-donut-chart',
    templateUrl: './dimension-donut-chart.component.html',
    styleUrls: ['./dimension-donut-chart.component.scss'],
})
export class DimensionDonutChartComponent implements OnInit {

    @Input() financialNotes: Category[];

    constructor() { }

    ngOnInit() {
        console.log(this.financialNotes);
    }

}
