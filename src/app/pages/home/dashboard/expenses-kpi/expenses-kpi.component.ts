import { Component, Input } from '@angular/core';
import { Category } from 'src/app/models/Category';
import { IFiltersModel } from 'src/app/models/IFiltersModel';
import { Entry } from 'src/app/models/Entry';
import _ from "lodash";

@Component({
    selector: 'app-expenses-kpi',
    templateUrl: './expenses-kpi.component.html',
    styleUrls: ['./expenses-kpi.component.scss'],
})
export class ExpensesKpiComponent {

    private financialNotes: Category[];
    private filtersModel: IFiltersModel[];
    public kpiValue : Number = 0;

    constructor() { }

    @Input() 
    set initComponent(data :{filteredData : Category[], filtersModel :IFiltersModel[]}) {
        this.financialNotes = data.filteredData;
        this.filtersModel = data.filtersModel;
        this.sumAllExpenses();
    }

    private sumAllExpenses(){
        this.kpiValue = _.chain(this.financialNotes)
            .map((category : Category) =>{
                return category.entries;
            })
            .flatten()
            .sumBy((entry : Entry)=>{
                return entry.value;
            })
            .value();
    }

}
