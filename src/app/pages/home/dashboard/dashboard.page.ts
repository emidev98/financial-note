import { Component, OnInit, OnDestroy } from '@angular/core';
import { NotifierService } from 'src/app/services/notifier.service';
import { DatabaseService } from 'src/app/services/database.service';
import { Category } from 'src/app/models/Category';
import { FiltersService } from 'src/app/services/filters.service';
import { Subscription } from 'rxjs';
import { IFiltersModel } from 'src/app/models/IFiltersModel';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.page.html',
    styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit, OnDestroy{

    public financialNotesData : {filteredData : Category[], filtersModel :IFiltersModel};
    private filtredDataSubscription : Subscription;

    constructor(
        private notifierService: NotifierService,
        private filtersService : FiltersService,
    ) { }

    ngOnInit() {
        this.notifierService.updateRouteTitle("DASHBOARD.TITLE", "dashboard");
        this.filtredDataSubscription = this.filtersService
            .filtredData
            .subscribe((response)=>{
                this.financialNotesData = response;
            });
        this.filtersService.applyFilters();
    }

    ngOnDestroy(){
        if(this.filtredDataSubscription) this.filtredDataSubscription.unsubscribe();
    }
}
