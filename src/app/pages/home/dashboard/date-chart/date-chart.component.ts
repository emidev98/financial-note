import { Component, OnInit, Input } from '@angular/core';
import { Category } from 'src/app/models/Category';

@Component({
    selector: 'app-date-chart',
    templateUrl: './date-chart.component.html',
    styleUrls: ['./date-chart.component.scss'],
})
export class DateChartComponent implements OnInit {

    @Input() financialNotes: Category[];

    constructor() { }

    ngOnInit() {
        console.log(this.financialNotes);
    }

}
