import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { MatSidenav, MatDialog } from '@angular/material';
import { DatabaseService } from 'src/app/services/database.service';
import { FirebaseApp } from '@angular/fire';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ModesService } from 'src/app/services/modes.service';
import { NotifierService } from 'src/app/services/notifier.service';
import { CategoryModalComponent } from 'src/app/components/category-modal/category-modal.component';
import { EntryModalComponent } from 'src/app/components/entry-modal/entry-modal.component';
import { Category } from 'src/app/models/Category';
import { Entry } from 'src/app/models/Entry';
import _ from "lodash";
import { TranslateService } from '@ngx-translate/core';
import { FiltersModalComponent } from 'src/app/components/filters-modal/filters-modal.component';
import { IFiltersModel } from 'src/app/models/IFiltersModel';

@Component({
    selector: 'app-home',
    templateUrl: './home.page.html',
    styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit, OnDestroy {

    @ViewChild("sidenav") private sidenav : MatSidenav;
    public reportsLoaded : boolean = false;
    public titleConfig   : { title? : string, icon? : string} = {};
    public isEditMode    : boolean = false;
    private getParsedDataCollectionsSubscription : Subscription;

    constructor(
        private firebase: FirebaseApp,
        private router : Router,
        private databaseService: DatabaseService,
        private modesService : ModesService,
        private notifierService : NotifierService,
        private translateService : TranslateService,
        public dialog: MatDialog
    ) { }

    ngOnInit() {
        this.getParsedDataCollectionsSubscription = this.databaseService.getParsedDataCollections().subscribe( () => this.reportsLoaded = true)
        this.modesService.onEditModeChanged.subscribe( isEditMode => this.isEditMode = isEditMode )
        this.notifierService.onUpdateRouteTitle.subscribe( topbarRoutingData  => this.titleConfig = topbarRoutingData);
    }

    public onClickToggleSidenav(){
        this.sidenav.toggle();
        
    }

    public onClickAddCategory(){
        const dialogRef = this.dialog.open(CategoryModalComponent,{
            data :{
                isNew : true,
                category : new Category()
            },
            maxWidth : "90vw"
        });

        dialogRef.afterClosed()
            .subscribe((category: Category) => {
                if(_.isUndefined(category)) return;

                this.databaseService
                    .createCategory(category)
                    .then((response)=>{
                        let message = this.translateService.instant("COMMON.CATEGORYSAVED",{ name : category.title});
                        this.notifierService.success(message);
                    },(err)=>{
                        let message = this.translateService.instant("COMMON.ERROR");
                        this.notifierService.error(message)
                    })
            });
    }

    public onClickAddEntry(){
        const dialogRef = this.dialog.open(EntryModalComponent,{
            data :{
                isNew : true,
                entry : new Entry()
            },
            maxWidth : "90vw"
        });

        dialogRef.afterClosed()
            .subscribe((entry: Entry) => {
                if(_.isUndefined(entry)) return;

                this.databaseService
                    .createEntry(entry)
                    .then((response)=>{
                        let message = this.translateService.instant("COMMON.ENTRYSAVED",{name : entry.description});
                        this.notifierService.success(message);
                    },(err)=>{
                        let message = this.translateService.instant("COMMON.ERROR");
                        this.notifierService.error(message)
                    })
            });
    }
    
    public onClickOpenFilters(){
        this.dialog.open(FiltersModalComponent,
            {
                width : "90vw",
                height : "90vh",
                maxWidth : "90vw",
                maxHeight : "90vh" 
            }
        );
    }
    
    public onClickClearFilters(){

    }

    public onClickOpenCamara(){
        console.log("openCamara()");
    }

    public onClickToggleEdit(){
        this.modesService.isEditMode = !this.modesService.isEditMode;
    }

    public onClickSaveEdit(){
        this.databaseService
            .saveChanges()
                .then(()=>{
                this.notifierService.success("COMMON.DATASAVEDSUCCESSFULLY");
                this.onClickToggleEdit();
            },()=>{
                
            })
    }

    public onClickCancelEdit(){
        this.databaseService.resetData();
        this.onClickToggleEdit();
    }


    public logout(){
        this.firebase
            .auth()
            .signOut()
            .then(()=>{
                if(this.getParsedDataCollectionsSubscription !== undefined){
                    this.getParsedDataCollectionsSubscription.unsubscribe();
                }
                this.router.navigate(["/guest"]);
                this.onClickToggleSidenav();
            },(error)=>{
                console.log(error);
            })
    }

    ngOnDestroy(){
        this.databaseService.ngOnDestroy();
    }
}
