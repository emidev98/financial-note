import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { DatabaseService } from 'src/app/services/database.service';
import { MatTabGroup } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { NotifierService } from 'src/app/services/notifier.service';
import { Router } from '@angular/router';


@Component({
    selector: 'app-guest',
    templateUrl: './guest.page.html',
    styleUrls: ['./guest.page.scss'],
})
export class GuestPage implements OnInit, AfterViewInit{

    @ViewChild("bottomTabGroup") private bottomTabGroup : MatTabGroup;
    public loginFormGroup : FormGroup;
    public registerFormGroup : FormGroup;
    public translates : Array<string>= [
        "COMMON.EMAIL",
        "COMMON.PASSWORD",
        "COMMON.REPEATPASSWORD"
    ];


    constructor(
        private translateService : TranslateService,
        private formBuilder: FormBuilder,
        private fireAuth: AngularFireAuth,
        private notifierService: NotifierService,
        private router : Router
    ) { }

    ngOnInit() {
        this.translateService.get(this.translates).subscribe((translates : Array<string>) => this.translates = translates);
        this.createFormControls();
    }

    ngAfterViewInit(){
        // This pice of code is because of a bug with material tabs,
        // the material tabs does not load correctly the first time.
        setTimeout(()=>this.bottomTabGroup.realignInkBar(),250)
    }

    private createFormControls(){
        this.loginFormGroup = this.formBuilder.group({
            email : ['', [Validators.required, Validators.email]],
            password : ['', [Validators.required, Validators.minLength(6)]]
        });

        this.registerFormGroup = this.formBuilder.group({
            email : ['', [Validators.required, Validators.email]],
            password : ['', [Validators.required, Validators.minLength(6)]],
            repeatPassword : ['', [Validators.required, Validators.minLength(6)]]
        }, 
        {
            validator: (g: FormGroup) => {
                return g.get('password').value === g.get('repeatPassword').value ? null : {'mismatch': true}
            }
        })
    }
    public login(){     
        this.fireAuth.auth
            .signInWithEmailAndPassword(this.loginFormGroup.get("email").value, this.loginFormGroup.get("password").value)
           .then( response => this.router.navigate(['home']))
           .catch( err => this.notifierService.error(err.message))
    }

    public register(){
        this.fireAuth.auth
            .createUserWithEmailAndPassword(this.registerFormGroup.get("email").value, this.registerFormGroup.get("password").value)
            .then( response => this.router.navigate(['home']))
            .catch( err => this.notifierService.error(err.message))
    }
    
}
