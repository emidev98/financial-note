import { NgModule } from '@angular/core';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './modules/routing.module';
import { MaterialModule } from './modules/material.module';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireDatabaseModule } from '@angular/fire/database';

import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';

import firebaseConfig from '../platform-config/firebase.json';

import { GuestPage } from './pages/guest/guest.page';
import { HomePage } from './pages/home/home.page';
import { DatabaseService } from './services/database.service';
import { DashboardPage } from './pages/home/dashboard/dashboard.page';
import { ReportsPage } from './pages/home/reports/reports.page';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NotifierService } from './services/notifier.service';
import { AuthGuard } from './services/auth-guard';
import { ModesService } from './services/modes.service';
import { CategoryModalComponent } from './components/category-modal/category-modal.component';
import { EntryModalComponent } from './components/entry-modal/entry-modal.component';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { ExpensesKpiComponent } from './pages/home/dashboard/expenses-kpi/expenses-kpi.component';
import { DateChartComponent } from './pages/home/dashboard/date-chart/date-chart.component';
import { DimensionChartComponent } from './pages/home/dashboard/dimension-chart/dimension-chart.component';
import { DimensionDonutChartComponent } from './pages/home/dashboard/dimension-donut-chart/dimension-donut-chart.component';
import { FiltersModalComponent } from './components/filters-modal/filters-modal.component';
import { FiltersService } from './services/filters.service';

@NgModule({ 
    declarations: [
        AppComponent,
        GuestPage,
        HomePage,
        DashboardPage,
        ReportsPage,
        CategoryModalComponent,
        EntryModalComponent,
        ExpensesKpiComponent,
        DateChartComponent,
        DimensionChartComponent,
        DimensionDonutChartComponent,
        FiltersModalComponent
    ],
    entryComponents: [
        CategoryModalComponent,
        EntryModalComponent,
        FiltersModalComponent
    ],
    imports: [
        IonicModule.forRoot(),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        AngularFireModule.initializeApp(firebaseConfig),
        AngularFireAuthModule,
        AngularFireDatabaseModule,
        AppRoutingModule,
        MaterialModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        ScrollDispatchModule
    ],
    providers: [
        AuthGuard,
        StatusBar,
        SplashScreen,
        DatabaseService,
        NotifierService,
        ModesService,
        FiltersService,
        {
            provide: RouteReuseStrategy,
            useClass: IonicRouteStrategy
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http);
}
